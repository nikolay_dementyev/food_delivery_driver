import 'dart:io';

////https://developer.apple.com/library/archive/featuredarticles/iPhoneURLScheme_Reference/MapLinks/MapLinks.html
////https://developers.google.com/maps/documentation/urls/guide

import 'package:flutter/material.dart';
// import 'package:maps_launcher/maps_launcher.dart';
import 'package:template/app/driver/bloc/index.dart';
import 'package:template/app/home/bloc/bloc.dart';
import 'package:template/models/order.dart';
import 'package:template/widgets/index.dart';
import 'package:url_launcher/url_launcher.dart';

class OrderStatusView extends StatefulWidget {
  
  final Order order;
  
  const OrderStatusView({Key key, this.order}) : super(key: key);
  @override
  _OrderStatusViewState createState() => _OrderStatusViewState();
}

class _OrderStatusViewState extends State<OrderStatusView> {
  
  List<String> itemList;
  bool enableDropdown = false;
  String _selectedOption;
  
  @override
  void initState() {
    super.initState();
    
    if(widget.order.status != null && widget.order.status.isNotEmpty && widget.order.status == 'Accepted') {
      itemList = [
        'Picked Up',
        'Cancel'
      ];
    } else if(widget.order.status != null && widget.order.status.isNotEmpty && widget.order.status == 'Picked Up') {
      itemList = ['Delivered'];
    }
    
    if(widget.order.status == 'Picked Up') {
      _getDistance();
    } else {
      enableDropdown = true;
    }
  }
  
  _getDistance() async {
    double distance = await BlocProvider.of<DriverBloc>(context).getDistanceToDestination(widget.order.deliveryLocation);
    if(distance <= 0.125) {
      setState(() {
        enableDropdown = true;
      });
    }
  }
  
  _onNavigate() async {
    if(widget.order.status == null || widget.order.status.isEmpty) return;
    
    if(widget.order.status == 'Accepted') {
      String curAddress = await BlocProvider.of<DriverBloc>(context).getCurAddress();
      String destAddress = widget.order.pickupLocation;
      // LatLng pickUplocation = await BlocProvider.of<DriverBloc>(context).getLatLng(widget.order.pickupLocation);
      curAddress = curAddress.replaceAll(' ', '%20');
      destAddress = destAddress.replaceAll(' ', '%20');
      print('https://maps.apple.com/?saddr=$curAddress&daddr=$destAddress&dirflg=d');
      // launch('https://maps.apple.com/?saddr=$curAddress&daddr=$destAddress&dirflg=d');
      if(Platform.isAndroid) {
        launch('https://www.google.com/maps/dir/?api=1&origin=$curAddress&destination=$destAddress&travelmode=driving');
      } else {
        launch('https://maps.apple.com/?saddr=$curAddress&daddr=$destAddress&dirflg=d');
      }
      // MapsLauncher.launchCoordinates(pickUplocation.latitude, pickUplocation.longitude);
    } else if(widget.order.status == 'Picked Up') {
      String curAddress = await BlocProvider.of<DriverBloc>(context).getCurAddress();
      String destAddress = widget.order.deliveryLocation;
      curAddress = curAddress.replaceAll(' ', '%20');
      destAddress = destAddress.replaceAll(' ', '%20');
      print('https://maps.apple.com/?saddr=$curAddress&daddr=$destAddress&dirflg=d');
      if(Platform.isAndroid) {
        launch('https://www.google.com/maps/dir/?api=1&origin=$curAddress&destination=$destAddress&travelmode=driving');
      } else {
        launch('https://maps.apple.com/?saddr=$curAddress&daddr=$destAddress&dirflg=d');
      }
      // NativeClient.call(curAddress, );
      // NativeClient.showDirection(curAddress, destAddress);
      // LatLng pickUplocation = await BlocProvider.of<DriverBloc>(context).getLatLng(widget.order.deliveryLocation);
      // MapsLauncher.launchCoordinates(pickUplocation.latitude, pickUplocation.longitude);
    }
  }
  
  _onCall() {
    print(widget.order.phoneNumber);
    // NativeClient.call(widget.order.phoneNumber);
    launch("tel://${widget.order.phoneNumber}");
  }
  
  _onSubmit() async {
    BlocProvider.of<DriverBloc>(context).add(ChangeStatus(widget.order.orderId, _selectedOption));
    Navigator.pop(context);
  }
  
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          width: 350,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Color(0xFF9803fc), width: 1.5),
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                width: 250,
                child: Column(children: <Widget>[
                   Disable(
                     isDisabled: !enableDropdown,
                     child: DropDownInputWidget(
                       itemList: itemList,
                       onChanged: (String value, int index) {
                         _selectedOption = value;
                       },
                  ),
                   ),
                  const SizedBox(height: 15),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset('assets/images/telephone.png', width: 50,),
                      const SizedBox(width: 10),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 2),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Color(0xFFcf975d),
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(5))
                        ),
                        child: InkWell(onTap: () {
                          _onCall();
                        }, child: Text('CONTACT CUSTOMER', style: TextStyle(fontSize: 12, color: Color(0xFF7c5945)),)),
                      )
                    ],
                  )
                ],),
              ),
              const SizedBox(height: 25),
              Row(
                children: <Widget>[
                  InkWell(onTap: () {
                    Navigator.pop(context);
                    _onNavigate();
                  }, child: Text('NAVIGATE', style: TextStyle(color: Colors.red, fontSize: 12))),
                  Spacer(),
                  InkWell(onTap: () {
                    Navigator.pop(context);
                  }, child: Text('CLOSE', style: TextStyle(color: Colors.red, fontSize: 12))),
                  const SizedBox(width: 20),
                  InkWell(onTap: () {
                    _onSubmit();
                  }, child: Text('OK', style: TextStyle(color: Colors.red, fontSize: 12))),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}