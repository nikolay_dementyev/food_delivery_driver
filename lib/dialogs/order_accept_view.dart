import 'package:flutter/material.dart';
import 'package:template/app/driver/bloc/index.dart';
import 'package:template/app/home/bloc/bloc.dart';
import 'package:template/models/order.dart';

class OrderAcceptView extends StatefulWidget {

  final Order order;
  
  const OrderAcceptView({Key key, this.order}) : super(key: key);
  @override
  _OrderAcceptViewState createState() => _OrderAcceptViewState();
}

class _OrderAcceptViewState extends State<OrderAcceptView> {
  
  String dialogText = '';
  
  bool canAcceptable = true;
  
  @override
  void initState() {
    super.initState();

    if (widget.order.status != null && widget.order.status.isNotEmpty && widget.order.status == "Not Assigned") {
        dialogText = "Pickup: "+ widget.order.pickupLocation +"\n\nDeliver To: "+ widget.order.deliveryLocation;
    } else {
        dialogText = "The order has been assigned!";
        canAcceptable = false;
    }
  }
  
  _onAccept() {
    BlocProvider.of<DriverBloc>(context).add(AcceptOrder(widget.order.orderId));
  }
  
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          width: 500,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Color(0xFF9803fc), width: 1.5),
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(dialogText, textAlign: TextAlign.left, style: TextStyle(color: Colors.grey, fontSize: 14)),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  InkWell(onTap: () {
                    Navigator.pop(context);
                  }, child: Text('CLOSE', style: TextStyle(color: Colors.red, fontSize: 12))),
                  const SizedBox(width: 20),
                  InkWell(onTap: () {
                    if(canAcceptable) {
                      Navigator.pop(context);
                      _onAccept();
                    }
                  }, child: Text('ACCEPT', style: TextStyle(color: canAcceptable ? Colors.red : Colors.grey, fontSize: 12))),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}