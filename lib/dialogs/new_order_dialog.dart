import 'package:flutter/material.dart';
import 'package:template/app/driver/bloc/index.dart';
import 'package:template/app/home/bloc/bloc.dart';
import 'package:template/models/order.dart';

class OrderRecievedView extends StatefulWidget {

  final Order order;
  
  const OrderRecievedView({Key key, this.order}) : super(key: key);
  @override
  _OrderAcceptViewState createState() => _OrderAcceptViewState();
}

class _OrderAcceptViewState extends State<OrderRecievedView> {
  
  String dialogText = '';
  
  _onAccept() {
    BlocProvider.of<DriverBloc>(context).add(AcceptOrder(widget.order.orderId));
  }
  
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          width: 400,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('New order received?', textAlign: TextAlign.left, style: TextStyle(color: Colors.black, fontSize: 14)),
              const SizedBox(height: 15),
              Text('Pickup location: ${widget.order.pickupLocation}', textAlign: TextAlign.left, style: TextStyle(color: Colors.black, fontSize: 14)),
              const SizedBox(height: 10),
              Text('Delivery location: ${widget.order.deliveryLocation}', textAlign: TextAlign.left, style: TextStyle(color: Colors.black, fontSize: 14)),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  InkWell(onTap: () {
                    Navigator.pop(context);
                  }, child: Text('CLOSE', style: TextStyle(color: Colors.red, fontSize: 14))),
                  const SizedBox(width: 20),
                  InkWell(onTap: () {
                      Navigator.pop(context);
                      _onAccept();
                  }, child: Text('ACCEPT', style: TextStyle(color: Colors.red, fontSize: 14))),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}