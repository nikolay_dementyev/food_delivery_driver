import 'package:firebase_auth/firebase_auth.dart';
import 'package:template/models/index.dart';
import 'package:template/singletons/index.dart';

import 'firebase_database.dart';

class FbAuthManager {
  
  FirebaseAuth _auth = FirebaseAuth.instance;
  
  static final FbAuthManager _instance = FbAuthManager._internal();
  
  FbAuthManager._internal();
  
  factory FbAuthManager() {
    return _instance;
  }
  
  // Log with email and password
  Future<Profile> signIn(String email, String password) async {
    try {
      AuthResult authResult = await _auth.signInWithEmailAndPassword(email: email, password: password); 
      FirebaseUser newUser = authResult.user;
      Profile profile = await FirebaseDatabaseClient.getUserDetail(newUser.uid);
      if(profile == null) return null;
      return profile;
    } catch (e) {
      Global().showToastMessage(e.message);
      return null;
    }
  }
  
  // Register with email and password
  Future<Profile> signUp(Profile profile) async {
    // AuthResult res = await _auth.createUserWithEmailAndPassword(email: profile.email, password: profile.password);
    // FirebaseUser newUser = res.user; 
    return Profile();
  }
  
  /// Sign out
  Future<bool> signOut({bool signOutWithGoogle = false}) async {
    try {
      await _auth.signOut();
      return true;
    } catch (e) {
      Global().showToastMessage(e.message);
      return false;
    }
  }
  
  /// Future
  // Future<Profile> signInWithGoogle() async {
    
    // try {
    //   GoogleSignIn googleSignIn = GoogleSignIn();
    //   GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    //   GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;
      
    //   AuthCredential authCredential = GoogleAuthProvider.getCredential(
    //     accessToken: googleSignInAuthentication.accessToken,
    //     idToken: googleSignInAuthentication.idToken
    //   );
      
    //   AuthResult authResult = await _auth.signInWithCredential(authCredential);
    //   FirebaseUser newUser = authResult.user;
    //   Profile profile = await FireStore.getUserDetail(newUser.uid);
      
    //   if(profile != null) {
    //     profile.isSingedWithGoogle = true;
    //     profile.email = newUser.email;
    //     profile.userName = newUser.displayName;
    //     return profile;
    //   }
      
    //   profile = Profile();
    //   profile.userId = newUser.uid;
    //   profile.aboutMe = 'I am a ...';
    //   profile.avatarUrl = null;
    //   profile.cityName = 'New York';
    //   profile.countryName = 'United States';
    //   profile.userName = newUser.displayName;
    //   profile.email = newUser.email;
    //   profile.isSingedWithGoogle = true;
    //   bool isCreated = await FireStore.createUserDetailDocument(profile);
    //   if(isCreated) {
    //     return profile;
    //   } else {
    //     return null;
    //   }
    // } catch (e) {
    //   return null;
    // }
  // }
}
