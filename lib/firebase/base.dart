import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

FirebaseAuth auth = FirebaseAuth.instance;
final db = Firestore.instance;
DatabaseReference realTimeDb = FirebaseDatabase.instance.reference();