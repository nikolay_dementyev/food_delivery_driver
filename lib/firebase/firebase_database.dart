import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:intl/intl.dart';
import 'package:template/models/index.dart';
import 'package:template/models/order.dart';
import 'base.dart'; 

class FirebaseDatabaseClient {
  
  static Future<Profile> getUserDetail(String userId) async {
    try {
      DataSnapshot dataSnapshot = await realTimeDb.child('Users').child(userId).once();
      print(dataSnapshot.key);
      print(dataSnapshot.value);
      // if(dataSnapshot. == null) return null;
      Profile profile = Profile.fromJson(dataSnapshot.value);
      profile.userId = userId;
      return profile;
    } catch (e) {
      return null;
    }
  }
  
  static StreamSubscription listenForNewOrder(String userId, {Function onNewOrder}) {
    DatabaseReference orderRef = realTimeDb.child('Orders');
    return orderRef.onChildAdded.listen((data) async {
      DatabaseReference authRestaurantsRef = realTimeDb.child('AuthorizedRestaurants');
      Order order = Order.fromJson(data.snapshot.value);
      order.orderId = data.snapshot.key;
       
      DatabaseReference cancelledOrdersRef = realTimeDb.child("CancelledOrders").child(userId).child(order.orderId);
      DataSnapshot cancelledOrderSnapshot = await cancelledOrdersRef.once();
      
      String curMonth = DateFormat('MM').format(DateTime.now());
      String curDay = DateFormat('dd').format(DateTime.now());
      
      String orderMonth = order.date != null ? order.date.split('/')[1] : 'xx';
      String orderDay = order.date != null ? order.date.split('/')[0] : 'xx';
      
      if(curMonth == orderMonth && curDay == orderDay) {
        if(order.status != null && order.status.isNotEmpty && order.status == 'Not Assigned') {
          DataSnapshot authRestosSnapshot = await authRestaurantsRef.once();
          Map authRestos = authRestosSnapshot.value as Map;
          for(int i = 0; i < authRestos.keys.length; i++) {
            if(userId == authRestos.keys.elementAt(i)) {
              String authStatus = authRestos[userId]['Status'];
                if(cancelledOrderSnapshot.value != 'Cancelled') {
                  if(authStatus != null && authStatus.isNotEmpty && authStatus == 'allowed all') {
                    if(onNewOrder != null) {
                      onNewOrder(order);
                    }
                  } else if(authStatus != null && authStatus.isNotEmpty && authStatus == 'custom') {
                    bool isFoundRestaurant = authRestos[userId][order.restaurantId] != null;
                    if(isFoundRestaurant) {
                      if(onNewOrder != null) {
                        onNewOrder(order);
                      }
                    }
                  }
                }
              }
          }
        }
      }
    }); 
  }
  
  static Future fetchAllOrders(String userId) async {
    
    DatabaseReference ordersRef = realTimeDb.child('Orders');
    DatabaseReference authRestaurantsRef = realTimeDb.child('AuthorizedRestaurants');
    
    DataSnapshot ordersSnapshot = await ordersRef.once();
    DataSnapshot authRestosSnapshot = await authRestaurantsRef.once();
    
    List<Order> orderList = [];
    Map authRestos = authRestosSnapshot.value as Map;
    Map orderMap = ordersSnapshot.value as Map;
    
    List<Order> filteredList = [];
    
    if(orderMap.keys.length != 0) {
      orderMap.forEach((key, value) {
        Order order = Order.fromJson(value);
        order.orderId = key;
        orderList.add(order);
      });
      for(int i = 0; i < orderList.length; i++) {
        Order order = orderList[i];
        String curMonth = DateFormat('MM').format(DateTime.now());
        String curDay = DateFormat('dd').format(DateTime.now());

        String orderMonth = order.date != null ? order.date.split('/')[1] : 'xx';
        String orderDay = order.date != null ? order.date.split('/')[0] : 'xx';
        
        if(curMonth == orderMonth && curDay == orderDay) {
          String authStatus = authRestos[userId]['Status'];
          if(authStatus != null && authStatus.isNotEmpty && authStatus == 'allowed all') {
            if(order.status != null && order.driverId != null && order.status.isNotEmpty && ((order.status == 'Not Assigned') || (order.status == 'Accepted') || (order.status == 'Picked Up'))) {
               filteredList.add(order);
            }
          } else if (authStatus != null && authStatus.isNotEmpty && authStatus == 'custom') {
            bool isFoundRestaurant = authRestos[userId][order.restaurantId] != null;
            if(isFoundRestaurant) {
              if(order.status != null && order.status.isNotEmpty && ((order.status == 'Not Assigned') || (order.status == 'Accepted' ) || (order.status == 'Picked Up'))) {
                filteredList.add(order);
              }
            }
          }
        }
      }
      return filteredList.isEmpty ? null : filteredList;
    } else {
      return null;
    }
  }
  
  static Future<List<Order>> fetchPickedOrders(String userId) async {

    DatabaseReference ordersRef = realTimeDb.child('Orders');
    
    DataSnapshot ordersSnapshot = await ordersRef.once();
    
    List<Order> orderList = [];
    Map orderMap = ordersSnapshot.value as Map;
    
    List<Order> filteredList = [];
    if(orderMap.keys.length != 0) {
      orderMap.forEach((key, value) {
        Order order = Order.fromJson(value);
        order.orderId = key;
        orderList.add(order);
      });
      for(int i = 0; i < orderList.length; i++) {
        
        Order order = orderList[i];
        String curMonth = DateFormat('MM').format(DateTime.now());
        String curDay = DateFormat('dd').format(DateTime.now());
        
        String orderMonth = order.date != null ? order.date.split('/')[1] : 'xx';
        String orderDay = order.date != null ? order.date.split('/')[0] : 'xx';
        
        if(curMonth == orderMonth && curDay == orderDay) {
          if(order.status != null && order.driverId != null && order.status.isNotEmpty && order.driverId.isNotEmpty && ((order.status == 'Accepted') || (order.status == 'Picked Up')) && userId == order.driverId) {
            filteredList.add(order);
          }
        }
      }
      return filteredList.isEmpty ? null : filteredList;
    } else {
      return null;
    }
  }
  
  static Future<bool> acceptOrder(String orderId, String myId, String myName) async {
    try {
      DatabaseReference orderRef = realTimeDb.child('Orders').child(orderId);
      await orderRef.child('status').set('Accepted');
      await orderRef.child('driver').set(myName);
      await orderRef.child('driverId').set(myId);
      return true;
    } catch (e) {
      return false;
    }
  }
  
  static Future<bool> changeOrderStatus(String userId, String orderId, String status) async {
    try {
      DatabaseReference orderRef = realTimeDb.child('Orders').child(orderId);
      String now = DateFormat('HH:mm:ss').format(DateTime.now());
      if(status == 'Picked Up') {
        await orderRef.child('status').set(status);
        await orderRef.child('timeOfPickup').set(now);
      } else if(status == 'Delivered') {
        await orderRef.child('status').set(status);
        await orderRef.child('timeOfCompletion').set(now);
      } else if(status == 'Cancel') {
        await orderRef.child('status').set('Not Assigned');
        await orderRef.child('timeOfPickup').set(now);
        await orderRef.child('driver').set('Waiting');
        await orderRef.child('driverId').set('-------');
        
        DatabaseReference cancelledOrdersRef = realTimeDb.child('CancelledOrders');
        cancelledOrdersRef.child(userId).child(orderId).set('Cancelled');
      }
      return true;
    } catch (e) {
      return false;
    }
  }
  
  static Future<bool> setOnline(String userId) async {
    try {
      DataSnapshot amOnline = await realTimeDb.child('.info/connected').once();
      DatabaseReference databaseReference = realTimeDb.child('Users').child(userId).child('Status');
      if(amOnline.value) {
         await databaseReference.onDisconnect().remove();
      }
      await databaseReference.set(true);
      return true;
    } catch (e) {
      return false;
    }
  }
}