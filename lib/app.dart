import 'package:template/app/driver/ui/home/driver_home.dart';
import 'package:template/app/home/bloc/bloc.dart';
import 'package:template/app/login/ui/login_page.dart';
import 'package:flutter/material.dart';
import 'app/home/ui/home_page.dart';
import 'app/restaurant/home/restaurant_home.dart';
import 'app/splash/splash_page.dart';
import 'blocs/index.dart';

class MyApp extends StatefulWidget {
  
  final GlobalKey<NavigatorState> navigatorKey;
  
  MyApp({Key key, @required this.navigatorKey}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
 
  @override
  void initState() {
    super.initState();

    BlocProvider.of<AuthenticationBloc>(context).add(AppStarted());
  }
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: widget.navigatorKey,
      theme: ThemeData(backgroundColor: Colors.white, scaffoldBackgroundColor: Colors.lightBlue),
      home: SplashPage(),
      routes: {
        '/home': (context) => HomePage(),
        '/login': (context) => LoginPage(),
        '/restaurantHome' : (context) => RestaurantHomePage(),
        '/driverHome' : (context) => DriverHomePage(),
      },
    );
  }
}