import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:template/app.dart';
import 'package:template/app/driver/bloc/driver_bloc.dart';
import 'package:template/app/driver/bloc/driver_event.dart';
import 'blocs/connectivity/connectivity_bloc.dart';
import 'blocs/index.dart';

void main() async {
  
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  final userRepository = UserRepository(sharedPreferences);
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey();
  // await LocalNotification().initialize();
  runApp(
   MultiBlocProvider(
      providers: [
        BlocProvider<NavigatorBloc>(
          create: (context) => NavigatorBloc(navigatorKey: _navigatorKey),
        ),
        BlocProvider<ConnectivityBloc>(
          create: (context) => ConnectivityBloc()..add(Listen()),
        ),
        BlocProvider<AuthenticationBloc>(
          create: (context) => AuthenticationBloc(userRepository, navigatorBloc: BlocProvider.of<NavigatorBloc>(context)),
        ),
        BlocProvider<LoadBloc>(
          create: (context) => LoadBloc(),
        ),
        BlocProvider<SubmitBloc>(
          create: (context) => SubmitBloc(),
        ),
        BlocProvider<DriverBloc>(
          create: (context) => DriverBloc(
            BlocProvider.of<NavigatorBloc>(context),
            loadBloc: BlocProvider.of<LoadBloc>(context),
            submitBloc: BlocProvider.of<SubmitBloc>(context),
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context)
          )..add(Initialize()),
        ),
      ],
      child: MyApp(navigatorKey: _navigatorKey),
    )
  );
}