import 'package:flutter/material.dart';

class InfoInput extends StatelessWidget {
  
  final TextEditingController controller;
  final Widget right;
  final IconData icon;
  final String hint;
  final TextInputType inputType;
  final bool obscureText;
  final ValueChanged<String> onChanged;
  
  const InfoInput({Key key, this.right, @required this.icon, @required this.hint, this.onChanged, this.controller, this.inputType = TextInputType.text, this.obscureText = false}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      constraints: BoxConstraints(
        maxWidth: 300
      ),
      child: TextField(
        keyboardType: inputType,
        obscureText: obscureText,
        controller: controller ?? TextEditingController(),
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          hintText: hint,
          border: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white)
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white)
          ),
          hintStyle: TextStyle(color: Colors.white, fontSize: 18)
        ),
        style: TextStyle(color: Colors.white, fontSize: 18),
        onChanged: (value) {
          if(onChanged != null) {
            onChanged(value);
          }
        },
      ),
    );
  }
}