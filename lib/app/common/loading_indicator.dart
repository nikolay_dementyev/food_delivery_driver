import 'package:flutter/material.dart';
import 'package:template/blocs/index.dart';

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SubmitBloc, SubmitState>(
      builder: (context, state) {
        return state is Submitting 
        ? Container(
          color: Color(0x55000000),
          child: Center(
            child: Container(width: 100, height: 100, color: Colors.white, child: Image.asset('assets/images/loader.gif')),
          ),
        )
        : Container();
      },
    );
  }
}