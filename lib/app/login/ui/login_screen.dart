import 'package:template/app/common/loading_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:template/app/login/bloc/bloc.dart';
import 'package:template/blocs/submit/bloc.dart';
import 'package:template/widgets/index.dart';
import '../../common/index.dart';

class LoginScreen extends StatefulWidget {
  
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  
  final TextEditingController _emailController = TextEditingController();
  
  final TextEditingController _passwordController = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Scaffold(
          resizeToAvoidBottomInset: true,
          body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color(0xFF8F30A9),
                  Color(0xFFBF317A),
                  Color(0xFF05ADCF),
                ],
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
              )
            ),
            child: SafeArea(
              child: Center(
                child: CustomScrollView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  slivers: <Widget>[
                    SliverToBoxAdapter(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Image.asset('assets/images/logo.png', width: 150, fit: BoxFit.fitWidth),
                          const SizedBox(height: 10),
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              InfoInput(controller: _emailController, icon: Icons.person, inputType: TextInputType.emailAddress, hint: 'john@email.com'),
                              const SizedBox(height: 25),
                              InfoInput(controller: _passwordController, icon: Icons.lock, hint: 'Password', obscureText: true),
                              const SizedBox(height: 40),
                              BlocBuilder<SubmitBloc, SubmitState>(
                                builder: (context, state) {
                                  return SubmitButton(title: 'LOG IN', onPressed: () {
                                    BlocProvider.of<LoginBloc>(context).add(Login(email: _emailController.text, password: _passwordController.text));
                                  });
                                },
                              ),
                            ],
                          ),
                        ]
                      ),
                    )
                  ],
                ),
              )
            ),
          ),
        ),
        LoadingIndicator()
      ],
    );
  }
}