import 'package:template/firebase/index.dart';
import '../../../models/index.dart';

class LoginRepository {
  
  Future<Profile> logIn(String email, String password) async {
    return await FbAuthManager().signIn(email, password);
  }
  
  Future<Profile> googleSignIn() async {
    return null;
  }
}