import 'package:template/app/profile/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:template/blocs/authentication/bloc.dart';
import 'package:template/widgets/index.dart';
import 'package:template/widgets/routes/scale_page_router.dart';

TextStyle menuItemStyle = TextStyle(color: Colors.grey, fontSize: 18);

class NavItem extends StatelessWidget {

  final VoidCallback onTap;
  final String title;
  final int delay;
  final IconData icon;
  const NavItem({Key key, this.onTap, this.title, this.delay = 300, @required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FadeIn(
      delay: delay,
      duration: Duration(milliseconds: 300),
      child: InkWell(
        onTap: () {
          onTap();
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 20, bottom: 20),
              child: Row(
                children: <Widget>[
                  const SizedBox(width: 50),
                  Icon(icon, size: 30, color: Colors.white),
                  const SizedBox(width: 20),
                  Text(title, style: TextStyle(color: Colors.white, fontSize: 18, height: 1),),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NavMenuWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      color: Colors.lightGreen,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 5, right: 45),
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                SizedBox(height: 33),
                NavItem(
                  icon: Icons.settings,
                  delay: 300,
                  title: 'Setting',                  
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                NavItem(
                  icon: Icons.person,
                  delay: 400,
                  title: 'Profile',
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, ScalePageRoute(builder: (context) => ProfilePage()));
                  },
                ),
                NavItem(
                  icon: Icons.home,
                  delay: 500,
                  title: 'Log out',
                  onTap: () {
                    Navigator.pop(context);
                    BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
                  },
                ),
                SizedBox(height: 50)
              ]),
            ),
          ],
        ),
      ),
    );
  }
}