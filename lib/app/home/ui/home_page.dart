import 'package:flutter/material.dart';
import 'package:template/app/home/bloc/bloc.dart';
import 'package:template/app/home/ui/home_screen.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => HomeBloc(),
        child: HomeScreen(),
      ),
    );
  }
}