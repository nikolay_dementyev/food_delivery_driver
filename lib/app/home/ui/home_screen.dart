import 'package:flutter/material.dart';
import 'package:template/app/home/ui/side_menu.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: Drawer(
        elevation: 5,
        child: NavMenuWidget()),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Builder(
              builder: (context) {
                return  Padding(
                  padding: const EdgeInsets.only(top: 10, left: 20),
                  child: InkWell(onTap: () {
                    Scaffold.of(context).openDrawer();
                  }, child: Icon(Icons.menu, color: Colors.black, size: 30,)),
                );
              },
            ),
          ]
        ),
      ),
    );
  }
}