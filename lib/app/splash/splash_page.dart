import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFF8F30A9),
              Color(0xFFBF317A),
              Color(0xFF05ADCF),
            ],
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
          )
        ),
        child: Center(
          child: FractionallySizedBox(
            widthFactor: 0.7,
            child: AspectRatio(aspectRatio: 1, child: Image.asset('assets/images/logo.png'))
          ),
        ),
      ),
    );
  }
}