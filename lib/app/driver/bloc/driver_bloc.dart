import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:template/app/driver/bloc/index.dart';
import 'package:template/blocs/index.dart';
import 'package:template/firebase/firebase_database.dart';
import 'package:template/models/index.dart';
import 'package:template/models/order.dart';
import 'package:template/singletons/global.dart';
import 'driver_state.dart';
import 'package:geocoder/geocoder.dart';
import 'driver_event.dart';
import 'package:template/app/driver/bloc/driver_model.dart';
import 'dart:math' as math;

double toRadians(double degrees) {
  return degrees * (math.pi / 180.0);
}

double toDegrees(double radians) {
  return radians * (180.0 / math.pi);
}
  
class DriverBloc extends Bloc<DriverEvent, DriverState> {
  
  final LoadBloc loadBloc;
  final SubmitBloc submitBloc;
  final AuthenticationBloc authenticationBloc;
  final NavigatorBloc navigatorBloc;
  
  List<Order> _allOrders; 
  List<Order> _pickedOrders; 
  
   /// My Current Position(Real Time Location)
  Position _myPosition;
  
  /// Location Change Subscription
  StreamSubscription<Position> _positionStreamSubscription;
  
  /// New Order Subscription
  StreamSubscription _newOrderSubscription;
  
  DriverBloc(this.navigatorBloc, {this.loadBloc, this.submitBloc, this.authenticationBloc});
  
  @override
  DriverState get initialState => DriverInitial();
  
  @override
  Stream<DriverState> mapEventToState(
    DriverEvent event,
  ) async* {
    if(event is Initialize) {
      yield* _mapInitializeToState();
    } else if(event is FetchOrders) {
      yield* _mapFetchToState();
      _listenNewOrder();
    } else if(event is ChangeStatus) {
      yield* _mapChangeToState(event);
    } else if(event is FetchMyOrders) {
      yield* _mapFetchPickedToState();
    } else if(event is AcceptOrder) {
      yield* _mapAcceptToState(event.orderId);
    } else if(event is AddOrder) {
      yield* _mapAddToState(event);
    }
  }
  
  @override
  Future<void> close() {
    _positionStreamSubscription?.cancel();
    _newOrderSubscription?.cancel();
    return super.close();
  }
  
  Stream<DriverState> _mapInitializeToState() async* {
    
    bool isPermitted = await Global().checkPermission(PermissionGroup.location);
    if(isPermitted) {
      /// Get Current Position
      _myPosition = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high).timeout(Duration(seconds: 20));
      
      /// Listen Location Change
      _listenLocationChange();
    }
  }
  
  _listenNewOrder() {
    if(_newOrderSubscription != null) return;

    _newOrderSubscription = FirebaseDatabaseClient.listenForNewOrder(
      authenticationBloc.state.profile.userId,
      onNewOrder: (Order order) {
        print('New Order Recieved : OrderId = ${order.orderId}');
        add(AddOrder(order));
      }
    );
  }
  
  Stream<DriverState> _mapAddToState(AddOrder event) async* {
    Order order = event.order;
    if(_allOrders != null) {
      Order _existingOrder = _allOrders.firstWhere((item) => item.orderId == order.orderId, orElse: () => null);
      if(_existingOrder != null) return;
    } 
    if(_pickedOrders != null) {
      Order _existingOrder = _pickedOrders.firstWhere((item) => item.orderId == order.orderId, orElse: () => null);
      if(_existingOrder != null) return;
    }
    if(_allOrders == null) {
      _allOrders = [order];
    } else {
      _allOrders.add(order);
    }
    _allOrders.sort((a, b) => a.toString().compareTo(b.toString()));
    yield NewOrderRecieved(order);
    await Future.delayed(Duration(milliseconds: 300));
    yield OrderListChanged(allOrders: _allOrders, pickedOrders: _pickedOrders);
  }
  
  Stream<DriverState> _mapFetchToState() async* {
    yield* _fetchAllOrders();
    yield* _mapFetchPickedToState();
  }
  
  Stream<DriverState> _fetchAllOrders() async* {
    if(!(await Global().checkPermission(PermissionGroup.location))) {
      return;
    }
    loadBloc.add(Load());
    _allOrders = await FirebaseDatabaseClient.fetchAllOrders(authenticationBloc.state.profile.userId);
    if(_allOrders != null) {
      _allOrders.sort((a, b) => a.toString().compareTo(b.toString()));
      loadBloc.add(LoadSucceeded());
      yield DriverInitial();
      yield OrderListChanged(allOrders: _allOrders);
    } else {
      loadBloc.add(LoadFailed());
    }
  }
  
  Stream<DriverState> _mapFetchPickedToState() async* {
    if(!(await Global().checkPermission(PermissionGroup.location))) {
      return;
    }
    loadBloc.add(Load());
    _pickedOrders = await FirebaseDatabaseClient.fetchPickedOrders(authenticationBloc.state.profile.userId);
    if(_pickedOrders != null) {
      _pickedOrders.sort((a, b) => a.toString().compareTo(b.toString()));
      loadBloc.add(LoadSucceeded());
      yield DriverInitial();
      yield OrderListChanged(allOrders: _allOrders, pickedOrders: _pickedOrders);
    } else {
      loadBloc.add(LoadFailed());
    }
  }
  
  Stream<DriverState> _mapChangeToState(ChangeStatus event) async* {
    try {
      submitBloc.add(Submit());
      bool isChanged = await FirebaseDatabaseClient.changeOrderStatus(authenticationBloc.state.profile.userId, event.orderId, event.status);
      if(isChanged) {
        submitBloc.add(SubmitSucceeded());
        yield* _mapFetchPickedToState();
      } else {
        submitBloc.add(SubmitFailed());
      }
    } catch (e) { 
      submitBloc.add(SubmitFailed());
    }
  }
  
  Stream<DriverState> _mapAcceptToState(String orderId) async* {
    submitBloc.add(Submit());
    Profile profile = authenticationBloc.state.profile;
    bool isAccpted = await FirebaseDatabaseClient.acceptOrder(orderId, profile.userId, profile.userName);
    if(isAccpted) {
      submitBloc.add(SubmitSucceeded());
      
      /// Remove from all order list and add to my orders list
      Order order = _allOrders.firstWhere((item) => item.orderId == orderId);
      order.status = 'Accepted';
      order.driver = profile.userName;
      order.driverId = profile.userId;
      if(_pickedOrders == null) {
        _pickedOrders = [order];
      } else {
        _pickedOrders.add(order);
      }
      _pickedOrders.sort((a, b) => a.toString().compareTo(b.toString()));
      _allOrders.sort((a, b) => a.toString().compareTo(b.toString()));
      
      // _allOrders.removeWhere((item) => item.orderId == orderId);
       
      yield DriverInitial();
      yield OrderListChanged(allOrders: _allOrders, pickedOrders: _pickedOrders);
    } else {
      submitBloc.add(SubmitFailed());
    }
  }
  
  Future<double> _getDriverDistance(String pickupLocation, String deliveryLocation) async {
    double kms = 0.0;
    LatLng latLng = await _getLatLng(pickupLocation);
    LatLng destLatLng =  await _getLatLng(deliveryLocation);
    double distance = 0.0;
    if(latLng != null && _myPosition != null && destLatLng != null) {
      distance = _getDistance(latLng, destLatLng);
    }
    kms = distance * 1.609;
    return kms;
  }
  
  /// Listen Location Change
  _listenLocationChange() {
    
    const LocationOptions locationOptions = LocationOptions(
      distanceFilter: 10,
      accuracy: LocationAccuracy.medium
    );
    
    final Stream<Position> positionStream = Geolocator().getPositionStream(locationOptions);
    
    _positionStreamSubscription = positionStream.listen((Position position) async {
      
      /// Save my current location
      _myPosition = position;
      
    });
  }
  
  Future<LatLng> _getLatLng(String location) async {
    try {
      List<Address> addresses = await Geocoder.local.findAddressesFromQuery(location);
      Address address = addresses.first;
      return LatLng(address.coordinates.latitude, address.coordinates.longitude);
    } catch (e) {
      return null;
    }
  }
  
  
  double _getDistance(LatLng src, LatLng dest) {
    double earthRadius = 3958.75;
    double destLat = toRadians(dest.latitude - src.latitude);
    double destLong = toRadians(dest.longitude - src.longitude);

    double sinDestLat = math.sin(destLat/2);
    double sinDestLong = math.sin(destLong/2);

    double a = math.pow(sinDestLat, 2) + math.pow(sinDestLong, 2) * math.cos(toRadians(src.latitude)) * math.cos(toRadians(dest.latitude));

    double c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a));
    double dist = earthRadius * c;
    return dist;
  }
  
  Future<String> getCurAddress() async {
    List<Address> addresses = await Geocoder.local.findAddressesFromCoordinates(Coordinates(_myPosition.latitude, _myPosition.longitude));
    Address address = addresses.first;
    return address.addressLine;
  }
  
  Future<double> getDistanceToDestination(String destLocation) async {
    if(_myPosition == null) return 10000;
    LatLng dest = await _getLatLng(destLocation);
    LatLng current = LatLng(_myPosition.latitude, _myPosition.longitude);
    return _getDistance(current, dest);
  }

  Future<LatLng> getLatLng(String location) async {
    return await _getLatLng(location);
  }
}
