import 'package:equatable/equatable.dart';
import 'package:template/models/order.dart';

abstract class DriverEvent extends Equatable {
  const DriverEvent();
  
  @override
  List<Object> get props => null;
}

class Initialize extends DriverEvent {}

class FetchOrders extends DriverEvent {}

class FetchMyOrders extends DriverEvent {}

class ChangeStatus extends DriverEvent {
  final String orderId;
  final String status;
  ChangeStatus(this.orderId, this.status);
}

class AddOrder extends DriverEvent {
  final Order order;

  AddOrder(this.order);
}

class AcceptOrder extends DriverEvent {
  final String orderId;
  AcceptOrder(this.orderId);
}