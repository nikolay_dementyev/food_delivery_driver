import 'package:equatable/equatable.dart';
import 'package:template/models/order.dart';

abstract class DriverState extends Equatable {
  const DriverState();

  @override
  List<Object> get props => null;
}

class DriverInitial extends DriverState {
  @override
  List<Object> get props => [];
}

class NewOrderRecieved extends DriverState {
  final Order order;

  NewOrderRecieved(this.order);
}

class OrderListChanged extends DriverState {
  final List<Order> allOrders;
  final List<Order> pickedOrders;
  OrderListChanged({this.allOrders, this.pickedOrders});
}