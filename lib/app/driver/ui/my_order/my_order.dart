import 'dart:async';

import 'package:flutter/material.dart';
import 'package:template/app/driver/bloc/index.dart';
import 'package:template/app/driver/ui/header_text.dart';
import 'package:template/blocs/index.dart';
import 'package:template/dialogs/order_status_view.dart';
import 'my_order_item.dart';

class MyOrders extends StatefulWidget {
  final VoidCallback onInitialized;

  const MyOrders({Key key, this.onInitialized}) : super(key: key);
  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrders> {

  @override
  void initState() {
    super.initState();

    Timer(Duration(milliseconds: 100), () {
       widget.onInitialized();
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        border: Border.all(color: Colors.blue)
      ),
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10)
              ),
              gradient: LinearGradient(
                colors: [
                  Color(0xFF0877F1),
                  Color(0xFF37A2EC)
                ],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight
              )
            ),
            child: Row(
              children: <Widget>[
                Expanded(child: HeaderText(title: 'Order No')),
                Expanded(child: HeaderText(title: 'Pickup')),
                Expanded(child: HeaderText(title: 'Delivery')),
                Expanded(child: HeaderText(title: 'Apart')),
                Expanded(child: HeaderText(title: 'Notes')),
                Expanded(child: HeaderText(title: 'Km\'s')),
                Expanded(child: HeaderText(title: 'Status')),
                Expanded(child: HeaderText(title: 'PM')),
              ],
            ),
          ),
          Expanded(
            child: BlocBuilder<DriverBloc, DriverState>(
              condition: ((prev, state) {
                return state is OrderListChanged;
              }),
              builder: (context, state) {
                if(state is OrderListChanged) {
                  return ListView.builder(
                    padding: EdgeInsets.all(0),
                    itemCount: state.pickedOrders?.length ?? 0,
                    itemBuilder: (context, int index) {
                      return MyOrderItem(order: state.pickedOrders[index], onTap: () {
                        showDialog(context: context, child: OrderStatusView(
                          order: state.pickedOrders[index]
                        ));
                      });
                    },
                  );
                } else {
                  return Container();
                }
              },
            )
          )
        ],
      ),
    );
  }
}