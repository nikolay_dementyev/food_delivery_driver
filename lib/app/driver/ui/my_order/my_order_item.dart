import 'package:flutter/material.dart';
import 'package:template/models/order.dart';

class MyOrderItemCell extends StatelessWidget {
  final String title;
  const MyOrderItemCell({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Expanded(child: Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Text(title, textAlign: TextAlign.center, style: TextStyle(color: Colors.black)),
    ));
  }
}

class MyOrderItem extends StatelessWidget {
  
  final Order order;
  final VoidCallback onTap;
  
  const MyOrderItem({Key key, this.order, this.onTap}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        children: <Widget>[
          MyOrderItemCell(title: order.orderNoSec),
          MyOrderItemCell(title: order.pickupLocation),
          MyOrderItemCell(title: order.deliveryLocation),
          MyOrderItemCell(title: order.apartment),
          MyOrderItemCell(title: order.notes),
          MyOrderItemCell(title: order.distance),
          MyOrderItemCell(title: order.status),
          MyOrderItemCell(title: order.paymentMethod.substring(0, 2)),
        ],
      ),
    );
  }
}