import 'dart:async';
import 'package:flutter/material.dart';
import 'package:template/app/driver/bloc/index.dart';
import 'package:template/app/driver/ui/header_text.dart';
import 'package:template/app/home/bloc/bloc.dart';
import 'package:template/blocs/index.dart';
import 'package:template/dialogs/order_accept_view.dart';
import 'package:template/firebase/firebase_database.dart';
import 'all_order_item.dart';

class AllOrders extends StatefulWidget {

  final VoidCallback onInitialized;

  const AllOrders({Key key, this.onInitialized}) : super(key: key);
  @override
  _AllOrdersState createState() => _AllOrdersState();
}

class _AllOrdersState extends State<AllOrders> {
    
  @override
  void initState() {
    super.initState();
    
    String userId = BlocProvider.of<AuthenticationBloc>(context).state.profile.userId;
    FirebaseDatabaseClient.fetchAllOrders(userId);

    Timer(Duration(milliseconds: 100), () {
       widget.onInitialized();
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        border: Border.all(color: Colors.blue)
      ),
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10)
              ),
              gradient: LinearGradient(
                colors: [
                  Color(0xFF0877F1),
                  Color(0xFF37A2EC)
                ],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight
              )
            ),
            child: Row(
              children: <Widget>[
                Expanded(child: HeaderText(title: 'Order No')),
                Expanded(child: HeaderText(title: 'Pickup')),
                Expanded(child: HeaderText(title: 'Delivery')),
                Expanded(child: HeaderText(title: 'Km\'s')),
                Expanded(child: HeaderText(title: 'Status')),
                Expanded(child: HeaderText(title: 'PM')),
              ],
            ),
          ),
          Expanded(
            child: BlocBuilder<DriverBloc, DriverState>(
              condition: ((prev, state) {
                return state is OrderListChanged;
              }),
              builder: (context, state) {
                if(state is OrderListChanged) {
                  return ListView.builder(
                    padding: EdgeInsets.all(0),
                    itemCount: state.allOrders?.length ?? 0,
                    itemBuilder: (context, int index) {
                      return AllOrderItem(order: state.allOrders[index], onTap: () {
                        showDialog(context: context, child: OrderAcceptView(
                          order: state.allOrders[index]
                        ));
                      });
                    },
                  );
                } else {
                  return Container();
                }
              },
            )
          )
        ],
      ),
    );
  }
}