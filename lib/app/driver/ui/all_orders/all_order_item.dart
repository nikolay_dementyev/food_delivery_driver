import 'package:flutter/material.dart';
import 'package:template/app/driver/ui/my_order/my_order_item.dart';
import 'package:template/models/order.dart';

class AllOrderItem extends StatelessWidget {
  
  final Order order;
  final VoidCallback onTap;
  
  const AllOrderItem({Key key, this.order, this.onTap}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        children: <Widget>[
          MyOrderItemCell(title: order.orderNoSec),
          MyOrderItemCell(title: order.pickupLocation),
          MyOrderItemCell(title: order.deliveryLocation),
          MyOrderItemCell(title: order.distance),
          MyOrderItemCell(title: order.status),
          MyOrderItemCell(title: order.paymentMethod.substring(0, 2)),
        ],
      ),
    );
  }
}