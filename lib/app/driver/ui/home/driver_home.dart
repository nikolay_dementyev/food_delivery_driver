import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:template/app/common/loading_indicator.dart';
import 'package:template/app/driver/bloc/index.dart';
import 'package:template/app/driver/ui/all_orders/all_order.dart';
import 'package:template/app/driver/ui/home/local_notification.dart';
import 'package:template/app/driver/ui/my_order/my_order.dart';
import 'package:template/app/home/bloc/bloc.dart';
import 'package:template/blocs/index.dart';
import 'package:template/dialogs/new_order_dialog.dart';
import 'package:template/fcm.dart';
import 'package:template/firebase/firebase_database.dart';
import 'package:template/models/order.dart';
import 'package:template/widgets/base/base_tab_bar_screen.dart';
import 'package:template/singletons/local_notification_manager.dart';

class DriverHomePage extends StatefulWidget {
  
  @override
  _DriverHomePageState createState() => _DriverHomePageState();
}

class _DriverHomePageState extends State<DriverHomePage> with WidgetsBindingObserver {
  
  int _index = 0;
  bool _isWorking = false;
  StreamSubscription _newOrderScription;
  
  List<Order> _newOrderList = [];
  
  Timer _timer;
                               
  @override
  void initState() {
    super.initState();
    
    WidgetsBinding.instance.addObserver(this);

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
    ]);
    
    FcmManager().setListener((token) {
      print(token);
    }, (notificationData) {
      print(notificationData);
    });
    
    /// Set Online on firebase database
    BlocProvider.of<AuthenticationBloc>(context).add(SetOnline());
    BlocProvider.of<DriverBloc>(context).add(FetchOrders());
    
    String userId = BlocProvider.of<AuthenticationBloc>(context).state.profile.userId;
    
    _newOrderScription = FirebaseDatabaseClient.listenForNewOrder(
      userId,
      onNewOrder: (Order order) async {
        _newOrderList.add(order);
      }
    );
    
    LocalNotificationManager.getInstance().initialize(null);
    
    _timer = Timer.periodic(Duration(seconds: 1), (timer) async {
      if(_newOrderList.isNotEmpty) {
        if(!_isWorking) {
          _isWorking = true;
          Order order = _newOrderList.first;
           
          LocalNotificationManager.getInstance().showNotification('New Order', 'new order has placed', 'payload');
          
          await showDialog(context: context, child: OrderRecievedView(order: order));
          // SoundManager().playNewMessageSound();
          _newOrderList.removeAt(0);
          _isWorking = false;
        }
      }
    });
  }
  
  AppLifecycleState _notification;
  
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
      _notification = state;
  }
   
  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _newOrderScription?.cancel();
    _timer?.cancel();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp
    ]);
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DriverBloc, DriverState>(
      builder: (context, state) {
        return Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Scaffold(
              backgroundColor: Colors.white,
              body: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).padding.top + 50,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color(0xFF0877F1),
                          Color(0xFF37A2EC)
                        ],
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight
                      )
                    ),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Text(_index == 0 ? 'All Placed Order' : 'Your Order', style: TextStyle(fontSize: 20, color: Colors.white,)),
                      ),
                    ),
                  ),
                  BlocBuilder<LoadBloc, LoadState>(
                    builder: (context, state) {
                      return state is Loading 
                      ? Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: CupertinoActivityIndicator(radius: 13,),
                      ) 
                      : Container();
                    },
                  ),
                  Expanded(
                    child: TabBarWidiget(onTablSelected: (int index) {
                      setState(() {
                        _index = index;
                      });
                    }),
                  )
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 15),
                child: FloatingActionButton(
                  backgroundColor: Color(0xFFcccccc),
                  child: Image.asset('assets/images/logout_icon.png'), onPressed: () {
                    BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
                  },
                ),
              ),
            ),
            LoadingIndicator()
          ],
        );
      },
    );
  }
}


class TabBarWidiget extends BaseTabBarWidget {
  
  final ValueChanged<int> onTablSelected;
  
  TabBarWidiget({this.onTablSelected});
  
  @override
  List<Widget> getChildren() {
    return [
      AllOrders(onInitialized: () {
        onTablSelected(0);
      }),
      MyOrders(
        onInitialized: () {
          onTablSelected(1);
        },
      ),
    ];
  }
  
  @override
  void onSelected(int index) {
    // onTablSelected(index);
  }
  
  @override
  List<IconData> getIconData() {
    return [
      Icons.format_list_bulleted,
      Icons.check_box
    ];
  }
}