
import 'package:flutter/material.dart';

class HeaderText extends StatelessWidget {
  final String title;

  const HeaderText({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(title, textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 16)),
    );
  }
}