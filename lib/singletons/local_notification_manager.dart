import 'package:flutter_local_notifications/flutter_local_notifications.dart';

typedef Future NotificationSelectCallback(String payload);

class LocalNotificationManager {
  
  static LocalNotificationManager _sharedInstance = LocalNotificationManager();
  
  static LocalNotificationManager getInstance() {
    return _sharedInstance;
  }
  
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  
  LocalNotificationManager() {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  }
  
  void initialize(NotificationSelectCallback onSelectNotification) {
    var initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }
  
  void showNotification(String title, String desc, String payload) {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    
    flutterLocalNotificationsPlugin.show(
        0, title, desc, platformChannelSpecifics,
        payload: payload);
  }
}