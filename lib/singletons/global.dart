
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';

class Global {

  static final Global _instance = Global._internal();
  
  factory Global() {
    return _instance;
  }
  
  Global._internal();
  
  void hideSoftKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }
  
  Future<bool> checkPermission(PermissionGroup permission) async {
    PermissionStatus permissionStatus = await PermissionHandler().checkPermissionStatus(permission);
    if(permissionStatus != PermissionStatus.granted) {
      Map<PermissionGroup, PermissionStatus> result = await PermissionHandler().requestPermissions([permission]);
      PermissionStatus locationPermissionStatus = result[PermissionGroup.location];
      if(locationPermissionStatus == PermissionStatus.granted) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }
  
  void showToastMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Color.fromRGBO(30, 30, 30, 0.6),
        textColor: Colors.white
    );
  }
  
  void showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }
}