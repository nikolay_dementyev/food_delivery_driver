import 'package:flutter/services.dart';
import 'dart:async';

class NativeClient {
  static const channel = const MethodChannel('busbookingapp/payment');

  static Future<Null> call(String phone) async {
    await channel.invokeMethod('call', [phone]);
  }
  
  static Future showDirection(String curAddress, String destAddress) async {
    await channel.invokeMethod('direction', [curAddress, destAddress]);
  }
  
  static Future<Null> showLoadingDialog(String msg) async {
    await channel.invokeMethod('showLoadingDialog', [msg]);
  }
  
  static Future<Null> hideLoadingDialog() async {
    await channel.invokeMethod('hideLoadingDialog');
  }
  
  static Future<Null> payment(List<String> params) async {
    await channel.invokeMethod('pay', params);
  }
}