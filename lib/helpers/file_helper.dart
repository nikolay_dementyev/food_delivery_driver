import 'dart:io';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';

/// Copy db file in the asset to the application directory
/// value of assetPath variable : 'assets/db/app.db'
/// target directory : application_directory/app.db
/// if app.db already exists, then does not copy.
/// Pick file of any type in mobile device

void pickFile({FileType pickingType = FileType.ANY, bool multiPick = false, Function(String) onPickedFile, Function(Map<String, String>) onPickedFiles, Function onError}) async {
   try {
    if(multiPick) {          
      Map<String, String> paths = await FilePicker.getMultiFilePath(
        type: pickingType,
      );
      if(onPickedFiles != null) {
        onPickedFiles(paths);
      }
    } else {         
      String path = await FilePicker.getFilePath(
        type: pickingType,
        // fileExtension: _extension
      );
      if(onPickedFile != null) {
        onPickedFile(path);
      }
    } 
  } on PlatformException catch (e) {
    if(onError != null) {
      onError(e);
    }
  }
}

Future<File> pickImage(ImageSource imageSource, {bool allowCroping = true}) async {
   File image = await ImagePicker.pickImage(source: imageSource);
   if(image == null) return null;
   if(allowCroping) {
    File croppedImage = await ImageCropper.cropImage(sourcePath: image.path,
      maxWidth: 512,
      maxHeight: 512,
    );
    return croppedImage;
   }
   return image;
}