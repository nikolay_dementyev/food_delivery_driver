///     Abstract Class For Models That Have Clone Function
///     copyWith function can clone new Model that has the same contents.
///     this remove the reference to the old object.
///     Usage
///     
///     class User extends BaseModel {
///       String name;
///       String email;
///       
///       User({this.name, this.email});
///  
///       @override
///       BaseModel copyWith() {
///         return User(email: this.email, name: this.name);
///       }
///     }
///     
///     User user = User(email: 'niklolay');
///     User cloneUser = user.copyWith();
/// 
///     01/15/2020 00:23 Last Edited By Jonh

abstract class BaseModel {
  BaseModel copyWith();
}