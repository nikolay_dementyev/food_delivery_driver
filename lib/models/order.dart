class Order {
  String orderId;
  String date;
  String orderNoSec;
  String notes;
  String timeOfPickup;
  String deliveryLocation;
  String pickupLocation;
  String restaurantId;
  String orderTime;
  String phoneNumber;
  String driver;
  String driverId;
  String timeOfCompletion;
  String paymentMethod;
  String apartment;
  String status;
  String distance;
  
  String toString() {
    switch (status) {
        case "Not Assigned":
            return "a";
        case "Accepted":
            return "b";
        case "Picked Up":
            return "c";
        case "Delivered":
            return "d";
        default:
            return "z";
    }
  }
  
  Order.fromJson(Map jsonMap) {
    date = jsonMap['date'];
    orderNoSec = jsonMap['orderNoSec'];
    notes = jsonMap['notes'];
    timeOfPickup = jsonMap['timeOfPickup'];
    deliveryLocation = jsonMap['deliveryLocation'];
    pickupLocation = jsonMap['pickupLocation'];
    restaurantId = jsonMap['restaurantId'];
    orderTime = jsonMap['orderTime'];
    phoneNumber = jsonMap['phoneNumber'];
    driver = jsonMap['driver'];
    driverId = jsonMap['driverId'];
    timeOfCompletion = jsonMap['timeOfCompletion'];
    paymentMethod = jsonMap['paymentMethod'];
    apartment = jsonMap['apartment'];
    status = jsonMap['status'];
    distance = jsonMap['distance'];
  }
}