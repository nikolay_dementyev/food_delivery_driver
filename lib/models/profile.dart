enum UserType {
  Driver,
  Restaurant
}

class Profile {
  
  //// Firebase UserId 
  String userId;
  UserType userType;
  String pickupLocation;
  String email;
  
  String userName;
  String password;
  
  Profile({this.userId, this.pickupLocation, this.userType, this.userName, this.password, this.email});
  
  Profile._internalFromJson(Map jsonMap) {
      pickupLocation = jsonMap['Location'];
      String typeStr = jsonMap['UserType'];
      email = jsonMap['Email'];
      if(typeStr == 'd') {
        userType = UserType.Driver;
      } else {
        userType = UserType.Restaurant;
      }
      userName = jsonMap['DisplayName'];
  } 
  
  factory Profile.fromJson(Map jsonMap) => Profile._internalFromJson(jsonMap);
}