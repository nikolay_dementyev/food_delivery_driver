import 'package:flutter/material.dart';

class SubmitButton extends StatelessWidget {
  
  final String title;
  final VoidCallback onPressed;
  const SubmitButton({Key key, @required this.title, this.onPressed}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
    onTap: onPressed,
    child: Container(
      margin: EdgeInsets.symmetric(horizontal: 50),
      constraints: BoxConstraints(
        minWidth: 50,
        maxWidth: 150
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        color: Color(0xFFb0439a)
      ),
      height: 50,
      child: Center(
        child: Text(title, style: TextStyle(fontSize: 18, color: Colors.white)),
      ),
    ),
  );
  }
}