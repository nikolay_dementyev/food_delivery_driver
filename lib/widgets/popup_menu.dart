import 'package:flutter/material.dart';

class PopupMenu extends StatelessWidget {

  final List<String> options;
  final ValueChanged<String> onSelected;
  final Widget child;
  final double vOffset;
  
  const PopupMenu({Key key, @required this.options, this.onSelected, this.child, this.vOffset = 0}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(
      child: child,
      onSelected: onSelected,
      offset: Offset(0, vOffset),
      itemBuilder: (context) {
        return List.generate(options.length, (int index) {
          return PopupMenuItem<String>(
            value: options[index],
            child: Text(options[index], style: TextStyle(fontSize: 20, color: Colors.black)),
          );
        });
      },
    );
  }
}