import 'package:flutter/material.dart';

class ScalePageRoute<T> extends MaterialPageRoute<T> {
  ScalePageRoute({
    WidgetBuilder builder,
    RouteSettings settings,
  }) : super(
          builder: builder,
          settings: settings,
        );
  
  @override Duration get transitionDuration => const Duration(milliseconds: 250);
  
  @override
  Widget buildTransitions(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    if (settings.isInitialRoute) {
      return child;
    }
    
    return ScaleTransition(
      scale: animation,
      child: child,
    );
  }
}
