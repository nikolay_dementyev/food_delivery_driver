import 'package:flutter/material.dart';
import 'package:template/blocs/index.dart';
import 'package:template/widgets/index.dart';

///     class PostList extends BaseListScreen {
///       @override
///       Widget buildListItem(BuildContext context, int index) {
///         return PostItem();
///       }
///  
///       @override
///       int getCount() {
///         return 10;
///       } 
///     }

abstract class BaseListScreen extends StatelessWidget {
  
  int getCount();
  
  Widget buildListItem(BuildContext context, int index);
  
  void onSearch(String query) {}
  
  bool showSearchBar() => true;
  
  void onSelected(BuildContext context, int index) {}
  
  void onLongPressed(BuildContext context, int index) {}
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            showSearchBar() 
            ? SearchBar(onQuery: (query) {
              onSearch(query);
            })
            : Container(),
             BlocBuilder<LoadBloc, LoadState>(
              builder: (context, state) {
                if(state is Loading) {
                  return LoadingBar();
                } else {
                  return Container();
                }
              },
            ),
            Expanded(
              child: ListView.builder(
                itemCount: getCount(),
                itemBuilder: (context, int index) => InkWell(onTap: () {
                  onSelected(context, index);
                }, onLongPress: () {
                  onLongPressed(context, index);
                }, child: buildListItem(context, index)),
              ),
            )
          ],
        ),
      ),
    );
  }
}