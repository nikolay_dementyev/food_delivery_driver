import 'package:flutter/material.dart';

///     Length of Titles Should be the same as the length of the Children
/// 
///     Usage 
/// 
///     class SettingScreen extends BaseTabBarScreen {
///       @override
///       List<Widget> getChildren() {
///         return [
///           MyOfferWidget(),
///           NewOfferWidget(),
///         ];
///       }
///
///       @override
///       List<String> getTitles() {
///         return [
///           'My Offers',
///           'New Offers'
///         ];
///       }  
///     }

abstract class BaseTabBarWidget extends StatelessWidget {
  
  // List<String> getTitles();
  
  List<Widget> getChildren();
  
  List<IconData> getIconData();
  
  int getInitialIndex() {
    return 0;
  }
  
  void onSelected(int index);
  
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return DefaultTabController(
      initialIndex: getInitialIndex(),
      length: getChildren().length,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              color: theme.primaryColor.withOpacity(.1),
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
                child: TabBarView(
                children: getChildren()
                ),
            ),
          ),
          TabBar(onTap: onSelected,
            tabs: List.generate(getChildren().length, (int index) {
              return Tab(icon: Icon(getIconData()[index]),);
            }),
            labelColor: theme.accentColor,
            indicatorColor: theme.accentColor,
            unselectedLabelColor: Colors.orange[200],
          )
        ],
      ),
    );
  }
}
