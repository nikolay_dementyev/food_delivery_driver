import 'package:flutter/material.dart';

///     Usage
///     class HomeScreen extends BaseBottomNavigationBarScreen {
///
///       @override
///       List<BottomNavigationBarData> getNavItems() {
///         return [
///           BottomNavigationBarData(
///             iconData: Mdi.tag,
///             title: 'Sell',
///           ),
///           BottomNavigationBarData(
///             iconData: Mdi.finance,
///             title: 'Dashboard',
///           ),
///           BottomNavigationBarData(
///             iconData: Mdi.tune,
///             title: 'Setting',
///           ),
///         ];
///       }
///
///       @override
///       List<Widget> buildChildren() {
///         return [
///           SellScreen(),
///           DashboardScreen(),
///           SettingScreen(),
///         ];
///       } 
///     }

class BottomNavigationBarData {
  
  final String title;
  final IconData iconData;
  
  BottomNavigationBarData({@required this.title, @required this.iconData});
}

abstract class BaseBottomNavigationBarScreen extends StatefulWidget {
  
  const BaseBottomNavigationBarScreen({Key key}) : super(key: key);
  
  @override
  _BaseBottomNavigationBarScreenState createState() => _BaseBottomNavigationBarScreenState();
  
  int getInitialIndex() {
    return 0;
  }
  
  AppBar buildAppBar(BuildContext context) {
    return null;
  }
  
  List<BottomNavigationBarData> getNavItems();
  
  List<Widget> buildChildren();
}

class _BaseBottomNavigationBarScreenState extends State<BaseBottomNavigationBarScreen> {
  
  int _selectedNavigationBarIndex;
  
  List<BottomNavigationBarItem> _bottomNavigationList ;
  
  @override
  void initState() {
    super.initState();
    
    _selectedNavigationBarIndex = widget.getInitialIndex();
    _bottomNavigationList = List.generate(widget.getNavItems().length, (int index) {
      return BottomNavigationBarItem(
        icon: Icon(widget.getNavItems()[index].iconData),
        title: Text(widget.getNavItems()[index].title),
      );
    });
  }
  
  void _onItemTapped(int index) {
    setState(() {
      _selectedNavigationBarIndex = index;
    });
  }
  
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: widget.buildAppBar(context),
      body: widget.buildChildren()[_selectedNavigationBarIndex] ?? Center(child: Text('Sorry, No child for this index', style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 18))),
      bottomNavigationBar: BottomNavigationBar(
        items: _bottomNavigationList,
        currentIndex: _selectedNavigationBarIndex,
        selectedItemColor: theme.accentColor,
        selectedIconTheme: theme.iconTheme,
        showUnselectedLabels: false,
        onTap: _onItemTapped,
      )
    );
  }
}