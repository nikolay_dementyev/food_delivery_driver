import 'package:flutter/material.dart';

class DropDownInputController {

  DropDownInputWidgetState _dropDownInputWidgetState;
  
  DropDownInputController(DropDownInputWidgetState dropDownInputWidgetState) {
    _dropDownInputWidgetState = dropDownInputWidgetState;
  }
  
  void setItems(List<String> items, {int initialIndex = 0}) {
    _dropDownInputWidgetState.setItems(items, initialIndex: initialIndex);
  }
}

typedef DropDownControllerCreatedCallback(DropDownInputController controller);

typedef OnSelectedCallback(String value, int index);

class DropDownInputWidget extends StatefulWidget {
  final DropDownControllerCreatedCallback onCreatedCallback;
  final String image;
  final List<String> itemList;
  final OnSelectedCallback onChanged;
  final String label;
  final bool isEnabled;
  final double paddingTop;
  final double paddingBottom;
  final IconData iconData;
  final String disableHint;
  final bool manualInput;
  final double fontSize;
  const DropDownInputWidget(
      {Key key,
      this.image,
      this.fontSize = 18,
      this.manualInput = false,
      this.paddingTop = 5,
      this.paddingBottom = 10,
      this.disableHint = '',
      this.isEnabled = true,
      this.iconData = Icons.arrow_drop_down,
      this.itemList,
      @required this.onChanged,
      this.label = '', this.onCreatedCallback})
      : super(key: key);
  
  @override
  DropDownInputWidgetState createState() {
    return DropDownInputWidgetState();
  }
}

class DropDownInputWidgetState extends State<DropDownInputWidget> {

  List<DropdownMenuItem<String>> itemsList = [];
  String selectedItem = '';
  bool isEnabled;
  DropDownInputController _controller;
  
  @override
  void initState() {
    super.initState();
    
    _controller = DropDownInputController(this);
    if(widget.onCreatedCallback != null) {
      widget.onCreatedCallback(_controller);
    }
    
    if(widget.itemList == null || widget.itemList.isEmpty)  return;
    
    isEnabled = true;
    itemsList = widget.itemList
        .map<DropdownMenuItem<String>>((str) => DropdownMenuItem<String>(
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(right: 4.0),
                child: Text(str,
                    maxLines: 2,
                    style: TextStyle(fontSize: widget.fontSize, height: 1),textAlign: TextAlign.right,),
              ),
            ),
            value: str))
        .toList();
    selectedItem = itemsList[0].value;
    widget.onChanged(selectedItem, 0);
  }
  
  void setItems(List<String> items, {int initialIndex}) {
    if(items == null || items.isEmpty) {
      setState(() {
        itemsList = null;
      });
      return;
    }
    items = items.toSet().toList();
    itemsList = items
        .map<DropdownMenuItem<String>>((str) => DropdownMenuItem<String>(
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(right: 4.0),
                child: Text(str,
                    maxLines: 2,
                    style: TextStyle(fontSize: widget.fontSize, height: 1),textAlign: TextAlign.right,),
              ),
            ),
            value: str))
        .toList();
    selectedItem = itemsList[initialIndex].value;
    widget.onChanged(selectedItem, initialIndex);
    setState(() {});
  }
  
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Row(children: <Widget>[
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: ButtonTheme(
                    child: DropdownButton(
                        icon: Icon(widget.iconData, size: 30, color: Colors.black),
                        isExpanded: true,
                        items: itemsList == null || itemsList.isEmpty ? null : itemsList,
                        value: selectedItem,
                        disabledHint: Text(
                          '$selectedItem',
                          style: TextStyle(fontSize: widget.fontSize),
                        ),
                        isDense: false,
                        onChanged: (value) {
                          setState(() {
                            selectedItem = value as String;
                          });
                          int index = itemsList.indexWhere((item) => item.value == value);
                          widget.onChanged(value as String, index);
                        }),
                  ),
                )
              ],
            ),
          ),
        ]),
      );
  }
}
