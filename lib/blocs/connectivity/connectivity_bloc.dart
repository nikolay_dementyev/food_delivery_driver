import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:equatable/equatable.dart';
part 'connectivity_event.dart';

class ConnectivityBloc extends Bloc<ConnectivityEvent, bool> {
  
  @override
  bool get initialState => false;
  
  Connectivity _connectivity;
  
  @override
  Stream<bool> mapEventToState(
    ConnectivityEvent event,
  ) async* {
    if(event is Listen) {
      _connectivity = Connectivity();
      final ConnectivityResult connectivityResult =
          await _connectivity.checkConnectivity();
      if (connectivityResult == ConnectivityResult.mobile) {
        yield true;
      } else if (connectivityResult == ConnectivityResult.wifi) {
        yield true;
      } else if (connectivityResult == ConnectivityResult.none) {
        yield false;
      } else {
        yield false;
      }
      _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
        if(result == ConnectivityResult.none) {
          if(!state) return;
          add(Disable());
          print('Network is not available');
        } else {
          if(state) return;
          add(Enable());
          print('Network is available');
        }
      }
    );
    } else if(event is Enable) {
      yield true;
    } else if(event is Disable) {
      yield false;
    }
  }
}