part of 'connectivity_bloc.dart';

abstract class ConnectivityEvent extends Equatable {
  const ConnectivityEvent();

  @override
  List<Object> get props => null;
}

class Listen extends ConnectivityEvent {}

class Enable extends ConnectivityEvent {}

class Disable extends ConnectivityEvent {}