import 'dart:async';
import 'package:bloc/bloc.dart';
import './bloc.dart';

class TimerBloc extends Bloc<TimerEvent, TimerState> {
  
  Timer _timer;
  int _duration;
  int _counter;
  
  @override
  TimerState get initialState => Stopped();
  
  @override
  Future<void> close() {
    _timer?.cancel();
    return super.close();
  }
  
  @override
  Stream<TimerState> mapEventToState(
    TimerEvent event,
  ) async* {
    if(event is Start) {
      _duration = event.duration;
      _startCounter();
    } else if(event is Stop) {
      _counter = 0;
      _timer?.cancel();
      yield Stopped();
    } else if(event is Tick) {
      print(event.ticks);
      yield Ticked(event.ticks);
      yield Running();
    } else if(event is Restart) {
      if(event.duration == -1) {
        _startCounter();
      } else {
        _duration = event.duration;
        _startCounter();
      }
    }
  }
  
  _startCounter() {
    add(Tick(_duration));
    _counter = _duration - 1;
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      add(Tick(_counter));
      _counter--;
      
      if(_counter < 0) {
        add(Stop());
      }
    });
  }
}
