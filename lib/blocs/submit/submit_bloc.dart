import 'dart:async';
import 'package:bloc/bloc.dart';
import './bloc.dart';

class SubmitBloc extends Bloc<SubmitEvent, SubmitState> {
  @override
  SubmitState get initialState => InitialSubmitState();
  
  @override
  Stream<SubmitState> mapEventToState(
    SubmitEvent event,
  ) async* {
    if(event is Submit) {
      yield Submitting();
    } else if(event is SubmitSucceeded) {
      yield SubmitSuccess();
      await Future.delayed(Duration(seconds: 1));
      yield InitialSubmitState();
    } else if(event is SubmitFailed) {
      yield SubmitFailure();
      await Future.delayed(Duration(seconds: 1));
      yield InitialSubmitState();
    }
  }
}
