import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:template/app/driver/ui/home/driver_home.dart';
import 'package:template/app/login/ui/login_page.dart';
import 'package:flutter/material.dart';
import 'package:template/app/home/ui/home_page.dart';
import 'package:template/app/restaurant/home/restaurant_home.dart';
import 'package:template/dialogs/new_order_dialog.dart';
import './bloc.dart';

class NavigatorBloc extends Bloc<NavigatorEvent, dynamic> {
  
  final GlobalKey<NavigatorState> navigatorKey;

  NavigatorBloc({@required this.navigatorKey}) : assert(navigatorKey != null);
  
  @override
  dynamic get initialState => null;
  
  @override
  Stream<dynamic> mapEventToState(
    NavigatorEvent event,
  ) async* {
    if(event is GoHome) {
      navigatorKey.currentState.pushAndRemoveUntil( 
        MaterialPageRoute(builder: (BuildContext context) => HomePage()),    
        ModalRoute.withName('/home')); 
    } else if(event is GoBack) {
      navigatorKey.currentState.pop();
    } else if(event is GoLoginPage) {
      navigatorKey.currentState.pushAndRemoveUntil( 
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()),    
        ModalRoute.withName('/login')); 
    } else if(event is GoDriverHome) {
       navigatorKey.currentState.pushAndRemoveUntil( 
        MaterialPageRoute(builder: (BuildContext context) => DriverHomePage()),    
        ModalRoute.withName('/driverHome')); 
    } else if(event is GoRestaurantHome) {
       navigatorKey.currentState.pushAndRemoveUntil( 
        MaterialPageRoute(builder: (BuildContext context) => RestaurantHomePage()),    
        ModalRoute.withName('/restaurantHome')); 
    } else if(event is ShowNewOrderDialog) {
      showDialog(context: navigatorKey.currentState.context, child: OrderRecievedView(order: event.order));
    }
  }
}