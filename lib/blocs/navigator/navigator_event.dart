import 'package:equatable/equatable.dart';
import 'package:template/models/order.dart';

abstract class NavigatorEvent extends Equatable {
  const NavigatorEvent();

  @override
  List<Object> get props => null;
}

class GoLoginPage extends NavigatorEvent {}
class GoHome extends NavigatorEvent {}
class GoBack extends NavigatorEvent {}
class GoDriverHome extends NavigatorEvent {}
class GoRestaurantHome extends NavigatorEvent {}
class ShowNewOrderDialog extends NavigatorEvent {
  final Order order;
  ShowNewOrderDialog(this.order);
}