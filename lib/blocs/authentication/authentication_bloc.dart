import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:template/blocs/authentication/authentication_state.dart';
import 'package:template/blocs/index.dart';
import 'package:template/fcm.dart';
import 'package:template/firebase/firebase_database.dart';
import 'package:template/singletons/global.dart';
import '../../models/index.dart';
import 'package:meta/meta.dart';
import 'bloc.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState> {
  
  final NavigatorBloc navigatorBloc;
  final UserRepository userRepository;
  Profile _profile;
  
  Timer _splashTimer;
  
  AuthenticationBloc(this.userRepository, {@required this.navigatorBloc});
  
  @override
  AuthenticationState get initialState => AuthenticationUninitialized(null);
  
  @override
  Future<void> close() {
    _splashTimer?.cancel();
    
    return super.close();
  } 
  
  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      _splashTimer = Timer.periodic(Duration(seconds: 3), (timer) {
        if(timer.tick == 1) {
          add(CheckAuthenticationStatus());
          _splashTimer.cancel();
        }
      });
    }
    if(event is CheckAuthenticationStatus) {
      final bool loggedIn = userRepository.isLogedIn();
      if (loggedIn) {
        _profile = userRepository.loadSession();
        yield AuthenticationAuthenticated(_profile);
        if(_profile.userType == UserType.Driver) {
          navigatorBloc.add(GoDriverHome());
        } else if(_profile.userType == UserType.Restaurant) {
          navigatorBloc.add(GoRestaurantHome());
        } else {
          Global().showToastMessage('You are an unauthorized user');
        }
      } else {
        yield AuthenticationUnauthenticated(null);
        navigatorBloc.add(GoLoginPage());
      }
    }
    if (event is LoggedIn) {
      if(state is AuthenticationAuthenticated) {
        yield AuthenticationUnauthenticated(null);
      }
      _profile = event.profile;
      yield AuthenticationAuthenticated(event.profile);
      
      userRepository.saveSession(event.profile);
      if(_profile.userType == UserType.Driver) {
        navigatorBloc.add(GoDriverHome());
        
        FcmManager().subscribeToTopic(TOPIC_BROADCAST);
        
      } else if(_profile.userType == UserType.Restaurant) {
        navigatorBloc.add(GoRestaurantHome());
      } else {
        Global().showToastMessage('You are an unauthorized user');
      }
    } else if(event is SetOnline) {
      FirebaseDatabaseClient.setOnline(_profile.userId);
    } else if (event is LoggedOut) {
      userRepository.deleteSession();
      yield AuthenticationUnauthenticated(null);
      navigatorBloc.add(GoLoginPage());
      
      FcmManager().unsubscribe(TOPIC_BROADCAST);
    }
  }
}