import 'package:shared_preferences/shared_preferences.dart';
import 'package:template/models/index.dart';

class UserRepository {
  
  final SharedPreferences sharedPrefs;
  
  UserRepository(this.sharedPrefs);
  
  bool isLogedIn() {
    return sharedPrefs.getBool('logged_in') ?? false;
  }
  
  Profile loadSession() {
    String fbUserId = sharedPrefs.getString('fb_user_id');
    String userName = sharedPrefs.getString('user_name');
    String pickupLocation = sharedPrefs.getString('pickup_location');
    String email = sharedPrefs.getString('email');
    String userTypeStr = sharedPrefs.getString('user_type');
    UserType userType;
    if(userTypeStr == 'driver') {
      userType = UserType.Driver;
    } else {
      userType = UserType.Restaurant;
    }
    Profile profile = Profile(userId: fbUserId, userType: userType, pickupLocation: pickupLocation, userName: userName, email: email);
    return profile;
  }
  
  void deleteSession() {
    sharedPrefs.setInt('user_id', null);
    sharedPrefs.setString('pickup_location', null);
    sharedPrefs.setString('user_type', null);
    sharedPrefs.setString('fb_user_id', null);
    sharedPrefs.setBool('logged_in', false);
    sharedPrefs.setString('user_name', null);
    sharedPrefs.setString('email', null);
  }
  
  void saveSession(Profile profile) {
    sharedPrefs.setString('pickup_location', profile.pickupLocation);
    sharedPrefs.setString('user_type', profile.userType == UserType.Driver ? 'driver' : 'restaurant');
    sharedPrefs.setBool('logged_in', true);
    sharedPrefs.setString('user_name', profile.userName);
    sharedPrefs.setString('email', profile.email);
    sharedPrefs.setString('fb_user_id', profile.userId);
  }
}