import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';

typedef void FCMTokenRefreshCallback(String fcmToken);
typedef void FCMMessageReceiveCallback(Map data);

const TOPIC_BROADCAST = 'broadcast';

class FcmManager {
  
  static FcmManager __instance = FcmManager._internal();
  
  FcmManager._internal() {
    
    _firebaseMessaging = FirebaseMessaging();
    
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    
    _firebaseMessaging.onTokenRefresh.listen((token) {
      if(_onTokenRefresh != null) {
         _onTokenRefresh(token);
      }
    });
    
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
            
            Map data;
            
            if(Platform.isIOS) {
              data = message;
            } else {
              data = message['data'] as Map;
            }
            if(_messageReceivedCallback != null) {
              _messageReceivedCallback(data);
            }
        },
      );
  } 
  
  factory FcmManager() {
    return __instance;
  }
  
    /// Token Refresh Listener
  FCMTokenRefreshCallback _onTokenRefresh;
  
  /// New Message Recieved Event Listener 
  FCMMessageReceiveCallback _messageReceivedCallback;
  
  FirebaseMessaging _firebaseMessaging;
  
  setListener(FCMTokenRefreshCallback onTokenRefresh, FCMMessageReceiveCallback onNotificationRecieved) {
    _onTokenRefresh = onTokenRefresh;
    print(' here.....');
    _messageReceivedCallback = onNotificationRecieved;
  }

  subscribeToTopic(String topic) {
    _firebaseMessaging.subscribeToTopic(topic);
  }
  
  unsubscribe(String topic) {
    _firebaseMessaging.unsubscribeFromTopic(topic);
  }
}